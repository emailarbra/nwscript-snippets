using System.Collections.Generic;

namespace NSSnippets
{
    public class FunctionHeader
    {
        public string Type { get; set; }
        public string Name { get; set; }
        public IEnumerable<string> Arguments { get; set; }
        public string[] Description { get; set; }
    }
}