using System.Collections.Generic;
using System.Text;

namespace NSSnippets
{
    public class JSONGenerator
    {
        private IEnumerable<ConstantHeader> constants;
        private IEnumerable<FunctionHeader> functions;

        public JSONGenerator(IEnumerable<ConstantHeader> constants, IEnumerable<FunctionHeader> functions)
        {
            this.constants = constants;
            this.functions = functions;
        }

        private string ConvertStringToPlaceholder(int num, string text)
        {
            return "${" + num.ToString() + ":" + text + "}";
        }
        
        private List<string> FunctionSnippet(FunctionHeader function)
        {
            StringBuilder body = new StringBuilder();
            body.Append(function.Name + "(");
            bool firstArg = true;
            int argNum = 1;
            foreach (var arg in function.Arguments)
            {
                if (firstArg == false)
                    body.Append(", ");
                body.Append(ConvertStringToPlaceholder(argNum++, arg).Replace("\"", "\\\""));
                firstArg = false;
            }
            body.Append(")");

            StringBuilder description = new StringBuilder();
            bool firstLine = true;
            foreach (var line in function.Description)
            {
                if (firstLine == false)
                    description.Append("\\n");
                description.Append(line.Replace("\"", "\\\""));
                firstLine = false;
            }

            var result = new List<string>
            {
                $"\"{function.Name}\": " + "{",
                $"\"prefix\": [\"{function.Name}\"],",
                $"\"body\": [\"{body}\"],",
                $"\"description\": [\"{description}\"]",
                "},"
            };
                
            return result;
        }
        
        private List<string> ConstantSnippet(ConstantHeader constant)
        {
            var result = new List<string>
            {
                $"\"{constant.Name}\": " + "{",
                $"\"prefix\": [\"{constant.Name}\"],",
                $"\"body\": [\"{constant.Body}\"],",
                $"\"description\": [\"{constant.Desc};\"]",
                "},"
            };
            return result;
        }
        
        public List<string> GenerateJSON()
        {
            var result = new List<string>();
            //result.Add("{");

            if (constants != null)
            {
                foreach (var constant in constants)
                    result.AddRange(ConstantSnippet(constant));
            }

            if (functions != null)
            {
                foreach (var function in functions)
                    result.AddRange(FunctionSnippet(function));
            }
            
            //result.Add("}");
            return result;
        }
    }
}