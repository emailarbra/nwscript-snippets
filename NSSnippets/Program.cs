﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace NSSnippets
{
    class Program
    {
        static int Main(string[] args)
        {
            if (args.Length < 4)
            {
                Console.WriteLine("Not enough arguments. Arguments should be the nwscript.nss path, the script path, the path to the directory containing includes (either directly or in subdirectories) and the path to nwscript.json file of Visual Studio Code.");
                return 1;
            }
            string nativeApiPath = args[0]; //"/home/taro94/Programy/NWScript Studio Code/data/nwscript-core/nwscript.nss";
            string scriptPath = args[1]; //"/home/taro94/Dokumenty/Realms/inc_arrays.nss";
            string includeDir = args[2]; //"/home/taro94/Dokumenty/Realms/";
            string jsonPath = args[3]; //"/home/taro94/Programy/NWScript Studio Code/data/user-data/User/snippets/nwscript.json";
            var result = new List<string>();
            result.Add("{");

            var errors = new List<Exception>();

            try{
                var apiGenerator = new JSONGenerator(new ConstantHeaders().GetConstants(), null);
                result.AddRange(apiGenerator.GenerateJSON());
            }
            catch(Exception e)
            {
                errors.Add(e);
            }

            var apiSource = File.ReadAllLines(nativeApiPath);
            try
            {
                var apiAnalyzer = new SourceAnalyzer(nativeApiPath, apiSource);
                var apiGenerator = new JSONGenerator(apiAnalyzer.Constants, apiAnalyzer.Functions);
                result.AddRange(apiGenerator.GenerateJSON());
            }
            catch (Exception e)
            {
                errors.Add(e);
            }

            var scriptSource = File.ReadAllLines(scriptPath);
            try
            {
                var scriptAnalyzer = new SourceAnalyzer(scriptPath, scriptSource);
                var scriptGenerator = new JSONGenerator(scriptAnalyzer.Constants, scriptAnalyzer.Functions);
                result.AddRange(scriptGenerator.GenerateJSON());
                foreach (var include in scriptAnalyzer.Includes)
                {
                    var includesFound = Directory.GetFiles(includeDir, include + ".nss", SearchOption.AllDirectories);
                    if (!includesFound.Any())
                        continue;
                    var includePath = includesFound[0];
                    var includeSource = File.ReadAllLines(includePath);
                    try
                    {
                        var includeAnalyzer = new SourceAnalyzer(includePath, includeSource);
                        var includeGenerator = new JSONGenerator(includeAnalyzer.Constants, includeAnalyzer.Functions);
                        result.AddRange(includeGenerator.GenerateJSON());
                    }
                    catch (Exception e)
                    {
                        errors.Add(e);
                    }
                }
            }
            catch (Exception e)
            {
                errors.Add(e);
            }

            result.Add("}");
            File.WriteAllLines(jsonPath, result);

            if (errors.Any())
            {
                foreach (var error in errors)
                    Console.WriteLine(error);
                return 1;
            }

            return 0;
        }
    }
}