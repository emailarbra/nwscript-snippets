namespace NSSnippets
{
    public static class NWScriptTypes
    {
        public const string STRING = "string";
        public const string INT = "int";
        public const string FLOAT = "float";
        public const string LOCATION = "location";
        public const string OBJECT = "object";
        public const string STRUCT = "struct";
        public const string EFFECT = "effect";
        public const string TALENT = "talent";
        public const string ITEMPROPERTY = "itemproperty";
        public const string EVENT = "event";
        public const string VOID = "void";
        public const string SQLQUERY = "sqlquery";
    }
}