﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NSSnippets
{
    public class ConstantHeaders
    {
        public IEnumerable<ConstantHeader> GetConstants()
        {
            //Control structures

            yield return new ConstantHeader
            {
                Name = "for",
                Body = "int ${1:nNth};\", \"for(${1:nNth} = 0; ${1:nNth} < ${2:nLength}; ${1:nNth}++) {\", \"\\t$0\", \"}",
                Desc = "A for loop",
                Type = string.Empty,
                Value = string.Empty,
            };

            yield return new ConstantHeader
            {
                Name = "while",
                Body = "while(${1:bCondition}) {\", \"\\t$0\", \"}",
                Desc = "A while loop",
                Type = string.Empty,
                Value = string.Empty,
            };

            yield return new ConstantHeader
            {
                Name = "do",
                Body = "do{\", \"\\t$0\", \"}while(${1:bCondition});",
                Desc = "A while loop",
                Type = string.Empty,
                Value = string.Empty,
            };

            yield return new ConstantHeader
            {
                Name = "switch",
                Body = "switch(${1:bCondition}){\", \"\\tcase ${2:CASEVALUE}:\" , \"\\t\\t$0\", \"\\t\\tbreak;\", \"\\tdefault:\" , \"\\t\\tbreak;\", \"}",
                Desc = "A while loop",
                Type = string.Empty,
                Value = string.Empty,
            };

            // Types

            foreach (System.Reflection.FieldInfo field in typeof(NWScriptTypes).GetFields())
            {
                string sTypeName = field.GetValue(null)?.ToString();

                if (string.IsNullOrWhiteSpace(sTypeName))
                {
                    continue;
                }

                yield return new ConstantHeader
                {
                    Name = sTypeName,
                    Body = sTypeName,
                    Desc = sTypeName,
                    Type = string.Empty,
                    Value = string.Empty,
                };
            }
        }
    }
}
