using System;
using System.Collections.Generic;
using System.Linq;
using static NSSnippets.NWScriptTypes;

namespace NSSnippets
{
    public class SourceAnalyzer
    {
        private string[] source;
        private List<ConstantHeader> constants;
        private List<FunctionHeader> functions;
        private List<string> includes = new List<string>();

        public List<ConstantHeader> Constants
        {
            get { return constants; }
            private set { constants = value; }
        }
        
        public List<FunctionHeader> Functions
        {
            get { return functions; }
            private set { functions = value; }
        }
        
        public List<string> Includes
        {
            get { return includes; }
            private set { includes = value; }
        }
        
        public SourceAnalyzer(string filename, string[] source)
        {
            this.source = source;
            constants = new List<ConstantHeader>();
            functions = new List<FunctionHeader>();
            if (filename.EndsWith("nwscript.nss"))
                GetCoreDefinitions();
            else
                GetDefinitions();
        }

        private bool CheckForInclude(string line)
        {
            if (line.StartsWith("#include "))
            {
                includes.Add(line.Split('"')[1]);
                return true;
            }

            return false;
        }
        
        private void AddConstantHeaderFromLine(string line)
        {
            line = line.Replace('=', ' '); //remove the equal sign
            line = line.Substring(0, Math.Max(line.IndexOf(';'), 0)); //get everything before the semicolon
            string[] splitLine = line.Split(' ', StringSplitOptions.RemoveEmptyEntries);
            constants.Add(new ConstantHeader
            {
                Type = splitLine[0],
                Name = splitLine[1],
                Value = splitLine[2]
            });
        }
        
        private void GetDefinitions()
        {
            bool inFuncDescription = false;
            List<string> funcDescriptionBuffer = new List<string>();
            foreach (var el in source)
            {
                if (el == string.Empty)
                {
                    inFuncDescription = false;
                    funcDescriptionBuffer = new List<string>();
                    continue;
                }

                var line = el.Replace('\t', ' ').Trim(' ');
                
                if (CheckForInclude(line))
                    continue;
                
                var lineType = GetLineType(line);
                if (lineType == "const")
                {
                    inFuncDescription = false;
                    funcDescriptionBuffer = new List<string>();
                    line = line.Substring(6).Trim(' ');
                    lineType = GetLineType(line);
                    AddConstantHeaderFromLine(line);
                }
                else if (line.StartsWith("//"))
                {
                    if (inFuncDescription == false)
                        funcDescriptionBuffer = new List<string>();
                    inFuncDescription = true;
                    line = el.Substring(2).Trim(' ');
                    funcDescriptionBuffer.Add(line);
                    continue;
                }
                else if (lineType != string.Empty)
                {
                    inFuncDescription = false;
                    line = line.Substring(0, Math.Max(line.IndexOf(';'), 0)); //get everything before the semicolon
                    if (line == string.Empty) //there was no semicolon at the end, so it's not a function header
                    {
                        funcDescriptionBuffer = new List<string>();
                        continue;
                    }
                    try
                    {
                        if (line.StartsWith("struct "))
                            line = line.Remove(0, 7);
                        string[] splitLine = line.Split('(', StringSplitOptions.RemoveEmptyEntries); //split into "int Func" and "int arg1, int arg2)"
                        string[] typeAndName = splitLine[0].Split(' ', StringSplitOptions.RemoveEmptyEntries);
                        if (typeAndName.Count() > 2 || typeAndName[1].Contains('=')) //this should never happen in function headers
                        {
                            funcDescriptionBuffer = new List<string>();
                            continue;
                        }
                        string[] arguments = splitLine[1].Substring(0, splitLine[1].Length - 1).Split(',', StringSplitOptions.RemoveEmptyEntries);
                        arguments = arguments.Select(x => x.Trim(new char[] {' ', '	'})).ToArray();
                        functions.Add(new FunctionHeader()
                        {
                            Type = typeAndName[0],
                            Name = typeAndName[1],
                            Description = funcDescriptionBuffer.ToArray(),
                            Arguments = arguments
                        });
                        funcDescriptionBuffer = new List<string>();
                    }
                    catch
                    {
                        funcDescriptionBuffer = new List<string>();
                        continue;
                    }
                }
            }
        }

        private void GetCoreDefinitions()
        {
            bool constantsSection = false;
            bool functionsSection = false;
            bool inFuncDescription = false;
            List<string> funcDescriptionBuffer = new List<string>();
            
            foreach (var el in source)
            {
                if (el == "// Constants")
                {
                    constantsSection = true;
                    continue;
                }
                if (el == "string sLanguage = \"nwscript\";")
                {
                    functionsSection = true;
                    constantsSection = false;
                    continue;
                }
                if (constantsSection)
                {
                    if (el == string.Empty)
                        continue;
                    var line = el.Replace('\t', ' ').Trim(' ');
                    var lineType = GetLineType(line);
                    if (lineType != string.Empty)
                        AddConstantHeaderFromLine(line);
                }
                else if (functionsSection)
                {
                    string line;
                    if (el == string.Empty)
                        continue;
                    if (el.StartsWith("//"))
                    {
                        if (inFuncDescription == false)
                            funcDescriptionBuffer = new List<string>();
                        inFuncDescription = true;
                        line = el.Substring(2).Replace('\t', ' ').Trim(' ');
                        funcDescriptionBuffer.Add(line);
                        continue;
                    }
                    line = el.Trim(' ');
                    var lineType = GetLineType(line);
                    if (lineType != string.Empty)
                    {
                        line = line.Substring(0, Math.Max(line.IndexOf(';'), 0)); //get everything before the semicolon
                        string[] splitLine = line.Split('(', StringSplitOptions.RemoveEmptyEntries); //split into "int Func" and "int arg1, int arg2)"
                        string[] typeAndName = splitLine[0].Split(' ', StringSplitOptions.RemoveEmptyEntries);
                        string[] arguments = splitLine[1].Substring(0, splitLine[1].Length - 1).Split(',', StringSplitOptions.RemoveEmptyEntries);
                        arguments = arguments.Select(x => x.Trim(new char[] {' ', '	'})).ToArray();
                        functions.Add(new FunctionHeader()
                        {
                            Type = typeAndName[0],
                            Name = typeAndName[1],
                            Description = funcDescriptionBuffer.ToArray(),
                            Arguments = arguments
                        });
                        funcDescriptionBuffer = new List<string>();
                        inFuncDescription = false;
                    }
                }
            }
        }

        private string GetLineType(string line)
        {
            if (line.StartsWith("const"))
                return "const";
            if (line.StartsWith(STRING))
                return STRING;
            if (line.StartsWith(INT))
                return INT;
            if (line.StartsWith(FLOAT))
                return FLOAT;
            if (line.StartsWith(VOID))
                return VOID;
            if (line.StartsWith(LOCATION))
                return LOCATION;
            if (line.StartsWith(OBJECT))
                return OBJECT;
            if (line.StartsWith(STRUCT))
                return STRUCT;
            if (line.StartsWith(EVENT))
                return EVENT;
            if (line.StartsWith(ITEMPROPERTY))
                return ITEMPROPERTY;
            if (line.StartsWith(EFFECT))
                return EFFECT;
            if (line.StartsWith(TALENT))
                return TALENT;
            if (line.StartsWith(SQLQUERY))
                return SQLQUERY;
            return string.Empty;
        }
    }
}