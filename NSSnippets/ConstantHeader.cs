namespace NSSnippets
{
    public class ConstantHeader
    {
        public string Type { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
        public string Body { get => body ?? Name; set => body = value; }
        public string Desc { get => desc ?? $"{Type} {Name} = {Value.Replace("\"", "\\\"")}"; set => desc = value; }

        private string desc;

        private string body;

        public ConstantHeader()
        {
            desc = null;
            body = null;
        }
    }
}